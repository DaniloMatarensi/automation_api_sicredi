# automation_api_sicredi
Teste Danilo Automation API Sicredi

# Como executar o projeto:

1 - Clonar o projeto para sua máquina
```git clone https://gitlab.com/DaniloMatarensi/automation_api_sicredi.git```

2 - Abrir na sua IDE preferencial

3 - Executar o arquivo "RunTest.java"


# Feedbacks:



Ao realizar o desenvolvimento da automação e executar, foi encontrado alguns bugs:

### 1 - As regras de negócio não estão funcionando, ex: permitiu cadastrar uma simulação com cpf com pontuação (POST).
**BUG CPF COM PONTUAÇÃO:**

![cpf com pontuacao](https://user-images.githubusercontent.com/20939192/157394053-78129220-37f9-4473-8986-cd3d9526efd5.PNG)

### 2 - As regras de negócio não estão funcionando, ex: permitiu alterar uma simulação com valor abaixo de 1000 reais.

**BUG CPF DUPLICADO ESTÁ RETORNANDO STATUS ERRADO:**

![cpf duplicado está retornando status errado](https://user-images.githubusercontent.com/20939192/157394070-9d70e8ee-3937-4744-89e5-b05862c7d7c0.PNG)

### 3 - O (DELETE) faz diversas vezes a mesma deleção de ID, deleta IDs inexistentes e isso diferente da regra de negócio.
